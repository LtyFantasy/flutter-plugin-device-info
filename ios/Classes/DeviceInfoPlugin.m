#import "DeviceInfoPlugin.h"
#import "TNDeviceInfo.h"
#import "TNNetworkInfo.h"
#import <UserNotifications/UserNotifications.h>

@interface DeviceInfoPlugin()

@property (nonatomic, strong) FlutterMethodChannel *channel;
@property (nonatomic, copy) NSString *token;

@end

@implementation DeviceInfoPlugin

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel
                                     methodChannelWithName:@"device_info_plugin"
                                     binaryMessenger:[registrar messenger]];
    DeviceInfoPlugin* instance = [[DeviceInfoPlugin alloc] initWithChannel:channel];
    [registrar addApplicationDelegate:instance];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (instancetype)initWithChannel:(FlutterMethodChannel*)channel {
    
    if (self = [super init]) {
        _channel = channel;
        [self notificationRegister];
    }

    return self;;
}

- (void)notificationRegister {
    
    dispatch_async(dispatch_get_main_queue(), ^() {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    });
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    const char *data = [deviceToken bytes];
    NSMutableString *ret = [NSMutableString string];
    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [ret appendFormat:@"%02.2hhx", data[i]];
    }
    _token = [ret copy];
    [_channel invokeMethod:@"onTokenRefresh" arguments:_token];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    if ([@"getPlatformVersion" isEqualToString:call.method]) {
        result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
    }
    else if([@"getDeviceInfo" isEqualToString:call.method]){
        result([TNDeviceInfo current]);
    }
    else if([@"getNetworkInfo" isEqualToString:call.method]){
        result([TNNetworkInfo info]);
    }
    else if([@"isMetricSystem" isEqualToString:call.method]){
        result(@([[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue]));
    }
    else if([@"getCountryCode" isEqualToString:call.method]){
        NSLocale *locale = [NSLocale currentLocale];
        NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
        result(countryCode);
    }
    else if ([@"isIPad" isEqualToString:call.method]) {

        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType containsString:@"iPad"]) {
            result(@(YES));
        }
        else {
            result(@(NO));
        }

    }
    else if ([@"isVpnOn" isEqualToString:call.method]) {
        
        result(@([TNNetworkInfo isNetworkVPNConnected]));
    }
    else if ([@"getIOSToken" isEqualToString:call.method]) {
        result(_token);
    }
    else {
        result(FlutterMethodNotImplemented);
    }
}

@end
