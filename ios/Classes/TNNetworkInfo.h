//
//  TNNetworkInfo.h
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Reachability/Reachability.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

NS_ASSUME_NONNULL_BEGIN

@interface TNNetworkInfo : NSObject
+ (NSString *)currentConnection;
+ (NSString *)currentCellular;
+ (CTCarrier *)currentCellularProvider;
+ (BOOL)isNetworkProxyConnected;
+ (BOOL)isNetworkVPNConnected;
+ (BOOL)isNetworkProxyOrVPNConnected;

+ (NSDictionary *)info;
@end

NS_ASSUME_NONNULL_END
