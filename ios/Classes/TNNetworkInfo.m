//
//  TNNetworkInfo.m
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "TNNetworkInfo.h"
#import "Reachability+WPF.h"
#import <ifaddrs.h>

BOOL _vpnFlag;

@implementation TNNetworkInfo

+ (NSString *)currentConnection{
    NSString *cellular = @"UNKNOWN";
    if([Reachability wpf_sharedInstance].isReachable == NO){
        cellular = @"NONE";
    }else if([Reachability wpf_sharedInstance].isReachableViaWWAN){
        cellular = @"WWAN";
    }else if([Reachability wpf_sharedInstance].isReachableViaWiFi){
        cellular = @"WiFi";
    }
    return cellular;
}

+ (NSString *)currentCellular{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    if(networkInfo.currentRadioAccessTechnology){
        NSString *network = [networkInfo.currentRadioAccessTechnology stringByReplacingOccurrencesOfString:@"CTRadioAccessTechnology" withString:@""];
        return network;
    }
    return nil;
}

+ (CTCarrier *)currentCellularProvider{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = networkInfo.subscriberCellularProvider;
    return carrier;
    
}

+ (BOOL)isNetworkProxyConnected{
    CFURLRef cfURL = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.google.com"];
    CFDictionaryRef cfSettings = CFNetworkCopySystemProxySettings();
    NSArray *proxies = (__bridge_transfer NSArray *)(CFNetworkCopyProxiesForURL(cfURL, cfSettings));
    CFRelease(cfSettings);
    
    BOOL isProxy = NO;
    for (NSDictionary *proxy in proxies) {
        NSString *proxyType = [proxy objectForKey:(NSString *)kCFProxyTypeKey];
        NSString *proxyHostName = [proxy objectForKey:(NSString *)kCFProxyHostNameKey];
        NSString *proxyProt = [proxy objectForKey:(NSString *)kCFProxyPortNumberKey];
        if ([proxyType isEqualToString:@"kCFProxyTypeNone"] == NO){
            NSLog(@"设置了代理 %@ %@:%@", proxyType, proxyHostName, proxyProt);
            isProxy = YES;
            break;
        }
    }
    return isProxy;
}
+ (BOOL)isNetworkVPNConnected{
    NSDictionary *settings = (__bridge_transfer NSDictionary *)(CFNetworkCopySystemProxySettings());
    
    BOOL isVPN = NO;
    NSArray *keys = [settings[@"__SCOPED__"] allKeys];
    for (NSString *key in keys) {
        if ([key containsString:@"tap"] ||
            [key containsString:@"tun"] ||
            [key containsString:@"ppp"] ||
            [key containsString:@"ipsec"]){
            isVPN = YES;
        }
    }
    
    return isVPN;
}
+ (BOOL)isNetworkProxyOrVPNConnected{
    return [TNNetworkInfo isNetworkProxyConnected] || [TNNetworkInfo isNetworkVPNConnected];
}


+ (NSDictionary *)info{
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    NSString *connection = [TNNetworkInfo currentConnection];
    NSString *cellular = [TNNetworkInfo currentCellular];
    CTCarrier *carrier = [TNNetworkInfo currentCellularProvider];
    
    if(connection) info[@"connection"] = connection;
    if(cellular) info[@"cellular"] = cellular;
    if(carrier != nil){
        NSMutableDictionary *carrierDict = [NSMutableDictionary dictionary];
        if(carrier.carrierName) carrierDict[@"carrierName"] = carrier.carrierName;
        if(carrier.isoCountryCode) carrierDict[@"isoCountryCode"] = [carrier.isoCountryCode uppercaseString];
        if(carrier.mobileCountryCode) carrierDict[@"mobileCountryCode"] = carrier.mobileCountryCode;
        if(carrier.mobileNetworkCode) carrierDict[@"mobileNetworkCode"] = carrier.mobileNetworkCode;
        
        if(carrierDict.count > 0) info[@"carrier"] = carrierDict;
    }
    info[@"proxy"] = @([TNNetworkInfo isNetworkProxyConnected]);
    info[@"vpn"] = @([TNNetworkInfo isNetworkVPNConnected]);
    return info;
}

+ (BOOL)isVPNOn {
    
    BOOL flag = NO;
    NSString *version = [UIDevice currentDevice].systemVersion;
    // 9.0以上版本
    if (version.doubleValue >= 9.0) {
        
        NSDictionary *dict = CFBridgingRelease(CFNetworkCopySystemProxySettings());
        NSArray *keys = [dict[@"__SCOPED__"] allKeys];
        for (NSString *key in keys) {
            if ([key rangeOfString:@"tap"].location != NSNotFound ||
                [key rangeOfString:@"tun"].location != NSNotFound ||
                [key rangeOfString:@"ipsec"].location != NSNotFound ||
                [key rangeOfString:@"ppp"].location != NSNotFound){
                flag = YES;
                break;
            }
        }
    }
    // 9.0以前版本
    else {
        
        struct ifaddrs *interfaces = NULL;
        struct ifaddrs *temp_addr = NULL;
        int success = 0;
        
        // retrieve the current interfaces - returns 0 on success
        success = getifaddrs(&interfaces);
        if (success == 0)
        {
            // Loop through linked list of interfaces
            temp_addr = interfaces;
            while (temp_addr != NULL)
            {
                NSString *string = [NSString stringWithFormat:@"%s" , temp_addr->ifa_name];
                if ([string rangeOfString:@"tap"].location != NSNotFound ||
                    [string rangeOfString:@"tun"].location != NSNotFound ||
                    [string rangeOfString:@"ipsec"].location != NSNotFound ||
                    [string rangeOfString:@"ppp"].location != NSNotFound)
                {
                    flag = YES;
                    break;
                }
                temp_addr = temp_addr->ifa_next;
            }
        }
        
        // Free memory
        freeifaddrs(interfaces);
    }
    
    if (_vpnFlag != flag)
    {
        // reset flag
        _vpnFlag = flag;
        
        /* 无需通知
        // post notification
        __weak __typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [[NSNotificationCenter defaultCenter] postNotificationName:kRRVPNStatusChangedNotification
                                                                object:strongSelf];
        });
         */
    }
    
    return flag;
}

@end
