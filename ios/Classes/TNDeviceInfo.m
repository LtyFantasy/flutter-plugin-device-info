//
//  DeviceInfo.m
//  Runner
//
//  Created by elvin on 2019/6/17.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "TNDeviceInfo.h"
#import <Security/Security.h>
#include <sys/sysctl.h>
#include <UIKit/UIKit.h>
#import <AdSupport/AdSupport.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <AdSupport/AdSupport.h>
#import <CommonCrypto/CommonDigest.h>
#import "TNNetworkInfo.h"

#define TNDeviceUUIDKey @"com.teamn.TNDeviceUUIDKey"
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define TNLastUUIDCreateTimeUUIDKey @"com.teamn.TNLastUUIDCreateTimeUUIDKey"


@interface NSString (MD5)
- (NSString *)md5String;
@end

@interface TNKeyChainHelper : NSObject
+ (OSStatus)saveService:(NSString *)service data:(id)data;
+ (id)loadService:(NSString *)service;
+ (void)deleteService:(NSString *)service;
@end


@implementation TNDeviceInfo

+ (NSDictionary *)current{
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    
    NSString *idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    if(idfa == nil || [idfa isEqualToString:@"00000000-0000-0000-0000-000000000000"]){
        idfa = nil;
    }else{
        info[@"idfa"] = idfa;
    }
    
    NSData *data = [TNKeyChainHelper loadService:TNDeviceUUIDKey];
    if(data){
        info[@"deviceId"]= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }else{
        if(idfa != nil){
            info[@"deviceId"] = idfa;
        }else{
            CFUUIDRef uuid = CFUUIDCreate(nil);
            NSString *uuidString = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuid));
            CFRelease(uuid);
            info[@"deviceId"] = uuidString;
        }
        [TNKeyChainHelper saveService:TNDeviceUUIDKey data:[info[@"deviceId"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    info[@"deviceIdCreateTime"] = @0;
    
    if (data != NULL)
    {// 非首次安装
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([userDefaults objectForKey:@"lastLoginType"] == NULL)
        {// 没有登录过
            NSData *recordTimeData = [TNKeyChainHelper loadService:TNLastUUIDCreateTimeUUIDKey];
            NSString *recordTime = [[NSString alloc] initWithData:recordTimeData encoding:NSUTF8StringEncoding];
            info[@"deviceIdCreateTime"] = @(recordTime.integerValue);
        }
    }
    
    // 直接写入时间
    NSInteger recordTime = (NSInteger)([[NSDate date] timeIntervalSince1970] * 1000);
    NSString *recordTimeStr = [NSString stringWithFormat:@"%ld", recordTime];
    [TNKeyChainHelper saveService:TNLastUUIDCreateTimeUUIDKey data:[recordTimeStr dataUsingEncoding:NSUTF8StringEncoding]];
    
    info[@"deviceType"] = IS_IPAD ? @2 : @1;//1=iphone, 2=ipad
    info[@"OSVersion"] = [UIDevice currentDevice].systemVersion;
    info[@"machine"] = [TNDeviceInfo currentDeviceMachine];
    info[@"supportTelephony"] = @([[UIDevice currentDevice].model isEqualToString:@"iPhone"]);
    
    NSDictionary *network = [TNNetworkInfo info];
    if(network) info[@"network"] = network;
    return info;
}


+ (NSString *)currentDeviceMachine{
    int mib[2];
    size_t len;
    char *machine;
    
    mib[0] = CTL_HW;
    mib[1] = HW_MACHINE;
    sysctl(mib, 2, NULL, &len, NULL, 0);
    machine = malloc(len);
    sysctl(mib, 2, machine, &len, NULL, 0);
    
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    
    return platform;
}


+ (NSString *)currentCellular{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    if(networkInfo.currentRadioAccessTechnology){
        NSString *network = [networkInfo.currentRadioAccessTechnology stringByReplacingOccurrencesOfString:@"CTRadioAccessTechnology" withString:@""];
        return network;
    }
    return nil;
}

+ (CTCarrier *)currentCellularProvider{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = networkInfo.subscriberCellularProvider;
    return carrier;
    
}

@end



@implementation NSString (MD5)

- (NSString *)md5String{
    const char *cStr = [self UTF8String];
    unsigned char result[32];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result);
    NSString *md5 = [NSString stringWithFormat:
                     @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                     result[0], result[1], result[2], result[3],
                     result[4], result[5], result[6], result[7],
                     result[8], result[9], result[10], result[11],
                     result[12], result[13], result[14], result[15]
                     ];
    return [md5 lowercaseString];
}
@end

@implementation TNKeyChainHelper

+ (NSMutableDictionary *)getKeychainDictionary:(NSString *)service {
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            (__bridge id)kSecClassGenericPassword,(__bridge id)kSecClass,
            (__bridge id)kSecAttrAccessibleAfterFirstUnlock,(__bridge id)kSecAttrAccessible,
            service, (__bridge id)kSecAttrService,
            service, (__bridge id)kSecAttrAccount,
            nil];
}

+ (OSStatus)saveService:(NSString *)service data:(id)data {
    NSMutableDictionary *dictionary = [self getKeychainDictionary:service];
    SecItemDelete((__bridge CFDictionaryRef)dictionary);
    [dictionary setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:(__bridge id)kSecValueData];
    return SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);
}

+ (id)loadService:(NSString *)service {
    id ret = nil;
    NSMutableDictionary *dictionary = [self getKeychainDictionary:service];
    [dictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [dictionary setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    CFDataRef data = NULL;

    OSStatus code = SecItemCopyMatching((__bridge CFDictionaryRef)dictionary, (CFTypeRef *)&data);
    if (code == noErr) {
        @try {
            ret = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)data];
        } @catch (NSException *e) {
            NSLog(@"TNKeyChainHelper loadService %@ failed: %@", service, e);
        } @finally {
        }
    }
    if (data)
        CFRelease(data);
    return ret;
}

+ (void)deleteService:(NSString *)service {
    NSMutableDictionary *dictionary = [self getKeychainDictionary:service];
    SecItemDelete((__bridge CFDictionaryRef)dictionary);
}

@end
