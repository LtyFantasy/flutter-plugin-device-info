#import <Reachability/Reachability.h>

@interface Reachability (WPF)
+ (void)wpf_setupWithHost:(NSString *)host;
+ (instancetype)wpf_sharedInstance;
@end
