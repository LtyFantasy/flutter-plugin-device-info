//
//  DeviceInfo.h
//  Runner
//
//  Created by elvin on 2019/6/17.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TNDeviceInfo : NSObject
+ (NSDictionary *)current;
@end

NS_ASSUME_NONNULL_END
