
#import "Reachability+WPF.h"

@implementation Reachability (WPF)


static NSString *_host = @"8.8.8.8";
+ (void)wpf_setupWithHost:(NSString *)host{
    _host = host;
}

static Reachability *_instance;
+ (instancetype)wpf_sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [Reachability reachabilityWithHostName:_host];
    });
    return _instance;
}

@end
