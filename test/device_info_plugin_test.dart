import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:device_info_plugin/device_info_plugin.dart';
import 'dart:convert';

void main() {
  const MethodChannel channel = MethodChannel('device_info_plugin');

  const networkInfo =
      r'{"connection":"WiFi","cellular":"WCDMA","proxy":1,"vpn":0,"carrier":{"carrierName":"China Mobile","isoCountryCode":"CN","mobileCountryCode":"460","mobileNetworkCode":"00"}}';
  const networkInfoWithoutCarrier =
      r'{"connection":"WiFi","cellular":"WCDMA","proxy":1,"vpn":0}';
  const deviceInfo =
      r'{"deviceId":"iea12bd7590fec7c811b7cd890ac95c2d","deviceType":1,"idfa":"B2C3D09F-F395-400A-8672-A122CF55EA9A","OSVersion":"12.2","machine":"x86_64","network":{"connection":"WiFi","cellular":"WCDMA","proxy":1,"vpn":0,"carrier":{"carrierName":"China Mobile","isoCountryCode":"CN","mobileCountryCode":"460","mobileNetworkCode":"00"}}}';

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getPlatformVersion') {
        return '42';
      } else if (methodCall.method == 'getNetworkInfo') {
        return jsonDecode(networkInfo);
      } else if (methodCall.method == 'getDeviceInfo') {
        return jsonDecode(deviceInfo);
      }

      throw UnimplementedError;
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await DeviceInfoPlugin.platformVersion, '42');
  });

  test('getNetworkInfo', () async {
    NetworkInfo? networkInfo = await DeviceInfoPlugin.networkInfo;
    expect(networkInfo?.connection, 'WiFi');
    expect(networkInfo?.cellular, 'WCDMA');
    expect(networkInfo?.proxy, true);
    expect(networkInfo?.vpn, false);
    expect(networkInfo?.carrier?.carrierName, 'China Mobile');
    expect(networkInfo?.carrier?.isoCountryCode, 'CN');
    expect(networkInfo?.carrier?.mobileCountryCode, '460');
    expect(networkInfo?.carrier?.mobileNetworkCode, '00');
  });


  test('getNetworkInfo without carrier', () async {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getNetworkInfo') {
        return jsonDecode(networkInfoWithoutCarrier);
      }

      throw UnimplementedError;
    });

    NetworkInfo? networkInfo = await DeviceInfoPlugin.networkInfo;
    expect(networkInfo?.carrier, null);
  });
}
