# device_info_plugin

A new Flutter plugin.

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

deviceInfo:
{
    "deviceId": "iea12bd7590fec7c811b7cd890ac95c2d",
    "deviceType": 1,
    "idfa": "B2C3D09F-F395-400A-8672-A122CF55EA9A",
    "OSVersion": "12.2",
    "machine": "x86_64",
    "network": $networkInfo
}

networkInfo:
{
    "connection": "WiFi",
    "cellular": "WCDMA",
    "proxy": 1,
    "vpn": 0,
    "carrier": {
        "carrierName": "China Mobile",
        "isoCountryCode": "CN",
        "mobileCountryCode": "460",
        "mobileNetworkCode": "00",
    }
}