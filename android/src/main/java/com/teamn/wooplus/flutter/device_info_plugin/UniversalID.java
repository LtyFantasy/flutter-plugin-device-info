package com.teamn.wooplus.flutter.device_info_plugin;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class UniversalID {

    private final static String mediaPrefix = "AP_device_";

    private final static String uuidPrefix = "UUID";

    private final static String createTimePrefix = "Time";

    private final static String SP_KEY = "UniversalID";

    private final static String SP_Ban_KEY = "banDevice";

    private static String filePath = File.separator + "UTips" + File.separator + "UUID";

    private static String createTimeFilePath = File.separator + "UTips" + File.separator + "CreateTime";

    private static String banFilePath = File.separator + "UTips" + File.separator + "banFilePath";

    //上一次uuid创建时间
    //只有非首次安装后首次打开有数据
    public static Long lastUUIDCreateTime = 0L;

    public static String getUniversalID(Context context) {

        //内部储存读取
        String uuid = PreferenceUtils.getAppPrefString(context, SP_KEY, "");

        //老用户第一次启动写入创建时间
        if (!TextUtils.isEmpty(uuid)) {
            double createTime = getUUIDCreateTime(context);
            if (createTime == 0) {
                saveUUIDCreateTime(context);
            }
        }
        Log.d("getUniversalID", "getUniversalID: "+uuid);
        //外部储存读取
        if (TextUtils.isEmpty(uuid)) {
            String pPath = getPath(context);
            if (!TextUtils.isEmpty(pPath)) {
                String fileRootPath = getPath(context) + filePath;
                uuid = FileUtils.readFile(fileRootPath);
                if (!TextUtils.isEmpty(uuid)) {
                    //本地文件有uuid，sp没有，说明为非首次安装
                    Long createTime = getUUIDCreateTime(context);
                    if (createTime != 0) {
                        lastUUIDCreateTime = createTime;
                    }
                    //更新安装时间
                    saveUUIDCreateTime(context);
                }
            }
        }
        //外部媒体数据库储存读取
        if (TextUtils.isEmpty(uuid)) {
            uuid = getStrFromMediaDB(context, uuidPrefix);
            if (!TextUtils.isEmpty(uuid)) {
                //本地媒体数据库有uuid，sp没有，说明为非首次安装
                Long createTime = getUUIDCreateTime(context);
                if (createTime != 0) {
                    lastUUIDCreateTime = createTime;
                }
                saveUUIDCreateTime(context);
            }
        }

        if (uuid == null || uuid.equals("") || uuid.equals("banDevice")) {
            uuid = UUID.randomUUID().toString();
        }
        if (!uuid.equals("")) {
            try {
                PreferenceUtils.setAppPrefString(context, SP_KEY, uuid);
                saveUUID(context, uuid);
            } catch (Exception e) {
                Log.d("uuid", "uuid save fail" + e.toString());
            }
        }
        return uuid;
    }

    public static long getUUIDCreateTime(Context context) {
        try {
            String pPath = getPath(context);
            if (!TextUtils.isEmpty(pPath)) {
                String fileRootPath = getPath(context) + createTimeFilePath;
                String createTime = FileUtils.readFile(fileRootPath).trim();
                Log.d("CreateTime", "getUUIDCreateTime file:" + Long.valueOf(createTime));
                return Long.valueOf(createTime);
            }

        } catch (Exception e) {
            Log.d("CreateTime", "getUUIDCreateTime file fail:" + e.toString());

        }

        try {
            String createTime = getStrFromMediaDB(context, createTimePrefix);
            Log.d("CreateTime", "getUUIDCreateTime media:" + Long.valueOf(createTime));

            return Long.valueOf(createTime);
        } catch (Exception e) {
            Log.d("CreateTime", "getUUIDCreateTime media fail:" + e.toString());
        }
        return 0;
    }

    public static void saveUUIDCreateTime(Context context) {
        try {
            String createTime = "" + System.currentTimeMillis();
            setStrFromMediaDB(context, createTime, createTimePrefix);
            String ExternalSdCardPath = getExternalSdCardPath() + createTimeFilePath;
            FileUtils.writeFile(ExternalSdCardPath, createTime);
            String InnerPath = context.getFilesDir().getAbsolutePath() + createTimeFilePath;
            FileUtils.writeFile(InnerPath, createTime);
            Log.d("CreateTime", "saveUUIDCreateTime:" + createTime);
        } catch (Exception e) {
            Log.d("CreateTime", "saveUUIDCreateTime fail :" + e.toString());
        }
    }

    ///大于Android10以上的设备，从媒体数据库读取设备码
    public static String getStrFromMediaDB(Context context, String prefix) {
        try {
            String[] projection = new String[]{
                    MediaStore.Audio.Media._ID,
                    MediaStore.Audio.Media.DISPLAY_NAME,
            };

            Cursor cursor = context.getApplicationContext().getContentResolver().query(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    projection,
                    null,
                    null,
                    null);
            int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
            int nameColumn =
                    cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME);
            while (cursor.moveToNext()) {
                // Get values of columns for a given video.
                long id = cursor.getLong(idColumn);
                String name = cursor.getString(nameColumn);
                if (name.startsWith(mediaPrefix + prefix)) {
                    String uuid = name.replace(mediaPrefix, "").replace(prefix, "").replace(".mp3", "")
                            .replace("Music", "");
                    Log.d("uuid", "getUUIDFromMediaDB success" + uuid + " \n " + name);
                    return uuid;
                }
            }
        } catch (Exception e) {
            Log.d("uuid", "getUUIDFromMediaDB fail" + e.toString());
        }

        return "";
    }

    ///大于Android10以上的设备，从媒体数据库读取设备码
    public static void setStrFromMediaDB(Context context, String saveString, String prefix) {
        try {
            ContentResolver resolver = context.getApplicationContext()
                    .getContentResolver();
            Uri audioCollection;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                audioCollection = MediaStore.Audio.Media.getContentUri(
                        MediaStore.VOLUME_EXTERNAL
                );
            } else {
                audioCollection = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }

            ContentValues newSongDetails = new ContentValues();
            String displayName = mediaPrefix + prefix + saveString + ".mp3";
            newSongDetails.put(MediaStore.Audio.Media.DISPLAY_NAME,
                    displayName);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                File directory = context.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
                newSongDetails.put(MediaStore.Audio.AudioColumns.DATA, directory.getPath() + displayName);
            }

            Uri myFavoriteSongUri = resolver
                    .insert(audioCollection, newSongDetails);

            Log.d("uuid", "setUUIDFromMediaDB success" + saveString);
        } catch (Exception e) {
            Log.d("uuid", "setUUIDFromMediaDB fail" + e.toString() + "\n" + e.getMessage());
        }
    }

    ///ban设备
    public static void banDevice(Context context) {
        try {
            String ExternalSdCardPath = getExternalSdCardPath() + banFilePath;
            FileUtils.writeFile(ExternalSdCardPath, "banDevice");
            String InnerPath = context.getFilesDir().getAbsolutePath() + banFilePath;
            FileUtils.writeFile(InnerPath, "banDevice");
        } catch (Exception e) {
            Log.d("uuid", "banDevice" + e.toString());
        }
        PreferenceUtils.setAppPrefString(context, SP_Ban_KEY, "banDevice");
    }

    ///clearBan设备
    public static void clearBanDevice(Context context) {
        try {
            String ExternalSdCardPath = getExternalSdCardPath() + banFilePath;
            FileUtils.writeFile(ExternalSdCardPath, "");
            String InnerPath = context.getFilesDir().getAbsolutePath() + banFilePath;
            FileUtils.writeFile(InnerPath, "");
        } catch (Exception e) {
            Log.d("uuid", "clearBanDevice" + e.toString());
        }
        PreferenceUtils.setAppPrefString(context, SP_Ban_KEY, "");
    }

    ///ban设备
    public static boolean isBanDevice(Context context) {
        //内部储存读取
        String uuid = PreferenceUtils.getAppPrefString(context, SP_Ban_KEY, "");
        //外部储存读取
        if (TextUtils.isEmpty(uuid)) {
            String pPath = getPath(context);
            if (!TextUtils.isEmpty(pPath)) {
                String fileRootPath = getPath(context) + banFilePath;
                uuid = FileUtils.readFile(fileRootPath);
            }
        }
        if (TextUtils.isEmpty(uuid)) {
            return false;
        }
        if (uuid.startsWith("banDevice")) {
            return true;
        }
        return false;
    }


    private static void saveUUID(Context context, String UUID) {
        setStrFromMediaDB(context, UUID, uuidPrefix);
        try {
            String ExternalSdCardPath = getExternalSdCardPath() + filePath;
            FileUtils.writeFile(ExternalSdCardPath, UUID);
            String InnerPath = context.getFilesDir().getAbsolutePath() + filePath;
            FileUtils.writeFile(InnerPath, UUID);
        } catch (Exception e) {
            Log.d("uuid", "uuid save file fail" + e.toString());
        }
    }

    public static String getPath(Context context) {
        //首先判断是否有外部存储卡，如没有判断是否有内部存储卡，如没有，继续读取应用程序所在存储
        String phonePicsPath = getExternalSdCardPath();
        if (phonePicsPath == null) {
            phonePicsPath = context.getFilesDir().getAbsolutePath();
        }
        return phonePicsPath;
    }

    /**
     * 遍历 "system/etc/vold.fstab” 文件，获取全部的Android的挂载点信息
     *
     * @return
     */
    private static ArrayList<String> getDevMountList() {
        String[] toSearch = FileUtils.readFile("/system/etc/vold.fstab").split(" ");
        ArrayList<String> out = new ArrayList<>();
        for (int i = 0; i < toSearch.length; i++) {
            if (toSearch[i].contains("dev_mount")) {
                if (new File(toSearch[i + 2]).exists()) {
                    out.add(toSearch[i + 2]);
                }
            }
        }
        return out;
    }

    /**
     * 获取扩展SD卡存储目录
     * <p/>
     * 如果有外接的SD卡，并且已挂载，则返回这个外置SD卡目录
     * 否则：返回内置SD卡目录
     *
     * @return
     */
    public static String getExternalSdCardPath() {

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File sdCardFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
            return sdCardFile.getAbsolutePath();
        }

        String path = null;

        File sdCardFile = null;

        ArrayList<String> devMountList = getDevMountList();

        for (String devMount : devMountList) {
            File file = new File(devMount);

            if (file.isDirectory() && file.canWrite()) {
                path = file.getAbsolutePath();

                String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
                File testWritable = new File(path, "test_" + timeStamp);

                if (testWritable.mkdirs()) {
                    testWritable.delete();
                } else {
                    path = null;
                }
            }
        }

        if (path != null) {
            sdCardFile = new File(path);
            return sdCardFile.getAbsolutePath();
        }

        return null;
    }

}
