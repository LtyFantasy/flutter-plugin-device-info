package com.teamn.wooplus.flutter.device_info_plugin;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * 用户退出后清除
 * Created by guotengqian on 2017/1/9 19:39.
 * def
 */

public class PreferenceUtils {

   static final String APP_SP_NAME="app_sp";
    public static Context checkContext(Context context){
        if(context==null){
            context= DeviceInfoPlugin.context;
        }
        return context;
    }

    public static String getPrefString(Context context, String sp_name, String key,
                                       final String defaultValue) {
        SharedPreferences settings = checkContext(context).getSharedPreferences(sp_name, Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }

    public static String getPrefString(Context context, String key,
                                       final String defaultValue) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        return settings.getString(key, defaultValue);
    }

    public static void setPrefString(Context context, final String key,
                                     final String value) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        settings.edit().putString(key, value).commit();
    }

    public static void setPrefString(Context context, String sp_name, final String key,
                                     final String value) {
        SharedPreferences settings = checkContext(context).getSharedPreferences(sp_name, Context.MODE_PRIVATE);
        settings.edit().putString(key, value).commit();
    }
    public static void setAppPrefString(Context context, final String key,
                                         final String value) {
        final SharedPreferences settings =checkContext(context).getSharedPreferences(APP_SP_NAME,Context.MODE_PRIVATE);
        settings.edit().putString(key, value).commit();
    }

    public static String getAppPrefString(Context context, final String key,
                                            final String defaultValue) {
        final SharedPreferences settings =checkContext(context).getSharedPreferences(APP_SP_NAME,Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }


    public static boolean getPrefBoolean(Context context, final String key,
                                         final boolean defaultValue) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));
        return settings.getBoolean(key, defaultValue);
    }
    public static boolean getAppPrefBoolean(Context context, final String key,
                                         final boolean defaultValue) {
        final SharedPreferences settings =checkContext(context).getSharedPreferences(APP_SP_NAME,Context.MODE_PRIVATE);
        return settings.getBoolean(key, defaultValue);
    }

    public static boolean hasKey(Context context, final String key) {
        return PreferenceManager.getDefaultSharedPreferences(checkContext(context)).contains(
                key);
    }

    public static void setPrefBoolean(Context context, final String key,
                                      final boolean value) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));

        settings.edit().putBoolean(key, value).commit();
    }
    public static void setAppPrefBoolean(Context context, final String key,
                                      final boolean value) {
        final SharedPreferences settings =checkContext(context).getSharedPreferences(APP_SP_NAME,Context.MODE_PRIVATE);
        settings.edit().putBoolean(key, value).commit();
    }

    public static void setPrefInt(Context context, final String key,
                                  final int value) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));
        settings.edit().putInt(key, value).commit();
    }

    public static int getPrefInt(Context context, final String key,
                                 final int defaultValue) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));
        return settings.getInt(key, defaultValue);
    }

    public static void setPrefFloat(Context context, final String key,
                                    final float value) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));
        settings.edit().putFloat(key, value).commit();
    }

    public static float getPrefFloat(Context context, final String key,
                                     final float defaultValue) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));
        return settings.getFloat(key, defaultValue);
    }

    public static void setPrefLong(Context context, final String key,
                                   final long value) {

        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));
        settings.edit().putLong(key, value).commit();
    }

    public static long getPrefLong(Context context, final String key,
                                   final long defaultValue) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));
        return settings.getLong(key, defaultValue);
    }

    public static void clearPreference(Context context,
                                       final SharedPreferences p) {
        final SharedPreferences.Editor editor = p.edit();
        editor.clear();
        editor.commit();
    }

    public static void clearPreference(Context context, String sp_name
    ) {
        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(checkContext(context));
        final SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

}