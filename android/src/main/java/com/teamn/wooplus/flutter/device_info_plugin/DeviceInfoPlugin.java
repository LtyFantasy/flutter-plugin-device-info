package com.teamn.wooplus.flutter.device_info_plugin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import static android.hardware.camera2.CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_3;
import static android.hardware.camera2.CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_FULL;
import static android.hardware.camera2.CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED;

/**
 * DeviceInfoPlugin
 */
public class DeviceInfoPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
    public static Context context;
    public static Activity activity;

    private MethodChannel channel;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "device_info_plugin");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("getDeviceInfo")) {
//      DeviceInfo.fromMap(Map map)
//      : deviceId = map['deviceId'],
//              deviceType = DeviceType(map['deviceType']),
//              idfa = map['idfa'],
//              osVersion = map['OSVersion'],
//              machine = map['machine'],
//              supportTelephony = map['supportTelephony'],
//              network =
//                      map['network'] != null ? NetworkInfo.fromMap(map['network']) : null,
//              isMetricSystem = map['isMetricSystem'];
            HashMap<String, Object> map = new HashMap();
            map.put("deviceId", UniversalID.getUniversalID(context).trim());
            map.put("deviceIdCreateTime", UniversalID.lastUUIDCreateTime);
            map.put("deviceType", 3);
            map.put("OSVersion", android.os.Build.VERSION.RELEASE);
            HashMap<String, Object> networkMap = new HashMap();
            networkMap.put("vpn", isVpnConnected() ? 1 : 0);
            map.put("network", networkMap);
            map.put("machine", android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL);
            result.success(map);
        } else if (call.method.equals("isMetricSystem")) {
            Locale locale = Locale.getDefault();
            String countryCode = locale.getCountry();

            result.success((!"US".equals(countryCode)) && (!"LR".equals(countryCode)) && (!"MM".equals(countryCode)));
        } else if (call.method.equals("isVpnOn")) {
            result.success(isVpnConnected());
        } else if (call.method.equals("getNetworkInfo")) {
            result.success(0);
        } else if (call.method.equals("getCountryCode")) {
            result.success(Locale.getDefault().getCountry());
        } else if (call.method.equals("banDevice")) {
            UniversalID.banDevice(context);
            result.success(null);
        } else if (call.method.equals("isBanDevice")) {
            result.success(UniversalID.isBanDevice(context));
        } else if (call.method.equals("clearBanDevice")) {
            UniversalID.clearBanDevice(context);
            result.success(null);
        } else if (call.method.equals("isFrontSupportAndroidCamera2")) {
            result.success(isHardwareLevelSupported());
        } else if (call.method.equals("openGpsSetting")) {
            Intent intent = new Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            activity.startActivityForResult(intent, 0);
            result.success(null);
        } else if (call.method.equals("jumpHome")) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_HOME);
            activity.startActivity(intent);
            result.success(null);
        } else if (call.method.equals("getSimData")) {
            try {
                result.success(getSimData());
            } catch (Exception e) {
                result.success(null);
            }
        } else if (call.method.equals("deviceSDKVersion")) {
            try {
                result.success(Build.VERSION.SDK_INT);
            } catch (Exception e) {
                result.success(null);
            }
        } else if (call.method.equals("deviceBrand")) {
            try {
                result.success(Build.BRAND);
            } catch (Exception e) {
                result.success(null);
            }
        } else {
            result.notImplemented();
        }
    }

    /**
     * @def [方法定义] VPN是否开启
     * @time 2017/9/26
     * @user EdenGuo
     */
    public static boolean isVpnConnected() {
        try {
            Enumeration<NetworkInterface> niList = NetworkInterface.getNetworkInterfaces();
            if (niList != null) {
                for (NetworkInterface intf : Collections.list(niList)) {
                    if (!intf.isUp() || intf.getInterfaceAddresses().size() == 0) {
                        continue;
                    }
                    if ("tun0".equals(intf.getName()) || "ppp0".equals(intf.getName())) {
                        return true;
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }

    private HashMap getSimData() throws Exception {
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        HashMap<String, String> data = new HashMap<>();
        String simOperator = tm.getSimOperator();
        if (simOperator != null && simOperator.length() >= 3) {
            data.put("mcc", tm.getSimOperator().substring(0, 3));
        }
        data.put("simIso", tm.getSimCountryIso());
        data.put("simName", tm.getSimOperatorName());
        data.put("networkIso", tm.getNetworkCountryIso());
        return data;
    }

    private boolean isHardwareLevelSupported() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            CameraManager cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            try {
                for (String cameraId : cameraManager.getCameraIdList()) {
                    CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                    if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT) {
                        int deviceLevel = cameraCharacteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
                        return deviceLevel == INFO_SUPPORTED_HARDWARE_LEVEL_3 ||
                                deviceLevel == INFO_SUPPORTED_HARDWARE_LEVEL_FULL ||
                                deviceLevel == INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return false;

    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding activityPluginBinding) {
        activity = activityPluginBinding.getActivity();
        context = activityPluginBinding.getActivity().getBaseContext();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        activity = null;
        context = null;
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding activityPluginBinding) {
        activity = activityPluginBinding.getActivity();
        context = activityPluginBinding.getActivity().getBaseContext();
    }

    @Override
    public void onDetachedFromActivity() {
        activity = null;
        context = null;
    }
}


