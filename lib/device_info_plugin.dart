import 'dart:async';
import 'dart:io';

import 'package:device_info_plugin/src/device_info.dart';
import 'package:device_info_plugin/src/network_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

export 'src/device_info.dart';
export 'src/network_info.dart';

class DeviceInfoPlugin {
  ValueChanged<String>? _onIOSTokenRefresh;

  static const MethodChannel _channel =
      const MethodChannel('device_info_plugin');

  static DeviceInfoPlugin get instance => _getInstance();
  static DeviceInfoPlugin? _instance;

  static DeviceInfoPlugin _getInstance() {
    if (_instance == null) {
      _instance = DeviceInfoPlugin._init();
    }
    return _instance!;
  }

  DeviceInfoPlugin._init() {
    _channel.setMethodCallHandler((call) async {
      /// iOS Token刷新
      if (call.method == 'onTokenRefresh') {
        _onIOSTokenRefresh?.call(call.arguments);
      }
    });
  }

  void setupIOSTokenRefreshCallback(ValueChanged<String> callback) {
    _onIOSTokenRefresh = callback;
  }

  static Future<String> get iOSDeviceToken async {
    final String token = await _channel.invokeMethod('getIOSToken');
    return token;
  }

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<DeviceInfo?> get deviceInfo async {
    final deviceInfo = await _channel.invokeMethod('getDeviceInfo');
    return deviceInfo != null ? DeviceInfo.fromMap(deviceInfo) : null;
  }

  //只有Android才有的函数
  static Future<bool> get isMetricSystem async {
    final bool = await _channel.invokeMethod('isMetricSystem');
    return bool;
  }

  ///前置摄像头是否支持Android camera2 api
  static Future<bool> get isFrontSupportAndroidCamera2 async {
    if (Platform.isIOS) return true;
    final bool = await _channel.invokeMethod('isFrontSupportAndroidCamera2');

    return bool;
  }

  /// 是否是iPad
  static Future<bool> get isIPad async {
    if (Platform.isIOS) {
      final bool = await _channel.invokeMethod('isIPad');
      return bool;
    }
    return false;
  }

  static Future<NetworkInfo?> get networkInfo async {
    final networkInfo = await _channel.invokeMethod('getNetworkInfo');
    return networkInfo != null ? NetworkInfo.fromMap(networkInfo) : null;
  }

  static Future<bool> get isVpnOn async {
    final isVpnOn = await _channel.invokeMethod('isVpnOn');
    return isVpnOn;
  }

  static Future<String> get countryCode async {
    String code = await _channel.invokeMethod('getCountryCode');
    return code;
  }

  static Future<String> get banDevice async {
    if (Platform.isIOS) return '';
    String code = await _channel.invokeMethod('banDevice');
    return code;
  }

  static Future<bool> get isBanDevice async {
    if (Platform.isIOS) return false;
    bool isBanDevice = await _channel.invokeMethod('isBanDevice');
    return isBanDevice;
  }

  static Future<bool> get clearBanDevice async {
    if (Platform.isIOS) return true;
    bool isBanDevice = await _channel.invokeMethod('clearBanDevice');
    return isBanDevice;
  }

  static Future<void> get openGpsSetting async =>
      await _channel.invokeMethod('openGpsSetting');

  static Future<SimData?> get getSimData async {
    final simData = await _channel.invokeMethod('getSimData');
    return simData != null ? SimData.fromMap(simData) : null;
  }

  ///进入home页面
  static Future<void> get jumpHome async =>
      await _channel.invokeMethod('jumpHome');

  /// [仅Android可用] 获取设备SDK版本
  static Future<int?> get deviceSDKVersion async {
    // 注：iOS暂未实现
    if (Platform.isIOS) return null;
    return await _channel.invokeMethod('deviceSDKVersion');
  }

  /// [仅Android可用] 获取设备品牌
  static Future<String?> get deviceBrand async {
    // 注：iOS暂未实现
    if (Platform.isIOS) return null;
    return await _channel.invokeMethod('deviceBrand');
  }
}

class SimData {
  ///sim卡服务商国家代码（美国卡在中国，返回美国）
  String? simIso;

  ///网络服务商国家代码（美国卡在中国，返回中国）
  /// ios没有这个值
  String? networkIso;

  ///运营商名称
  String? simName;

  ///运营商名称
  String? mcc;

  SimData({this.simIso, this.simName, this.mcc});

  SimData.fromMap(Map<dynamic, dynamic> map)
      : simIso = map['simIso'],
        networkIso = map['networkIso'],
        simName = map['simName'],
        mcc = map['mcc'];
}
