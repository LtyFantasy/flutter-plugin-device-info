class NetworkConnect {
  static const Unknown = "UNKNOWN";
  static const None = "NONE";
  static const Wifi = "Wifi";
  static const WWAN = "WWAN";
}

class NetworkInfo {
  final String? connection;
  final String? cellular;
  final bool? proxy;
  final bool? vpn;
  final CarrierInfo? carrier;

  NetworkInfo({
    this.connection,
    this.cellular,
    this.proxy,
    this.vpn,
    this.carrier,
  });

  NetworkInfo.fromMap(Map<dynamic, dynamic> map)
      : connection = map['connection'],
        cellular = map['cellular'],
        proxy = map['proxy'] == 1,
        vpn = map['vpn'] == 1,
        carrier =
            map['carrier'] != null ? CarrierInfo.fromMap(map['carrier']) : null;
}

class CarrierInfo {
  final String? carrierName;
  final String? isoCountryCode;
  final String? mobileCountryCode;
  final String? mobileNetworkCode;

  CarrierInfo({
    this.carrierName,
    this.isoCountryCode,
    this.mobileCountryCode,
    this.mobileNetworkCode,
  });

  CarrierInfo.fromMap(Map<dynamic, dynamic> map)
      : carrierName = map['carrierName'],
        isoCountryCode = map['isoCountryCode'],
        mobileCountryCode = map['mobileCountryCode'],
        mobileNetworkCode = map['mobileNetworkCode'];
}
