import 'network_info.dart';

class DeviceType {
  final int value;

  const DeviceType(this.value);

  const DeviceType.iPhone() : value = 1;

  const DeviceType.iPad() : value = 2;

  const DeviceType.android() : value = 3;

  @override
  bool operator ==(other) {
    return other is DeviceType && this.value == other.value;
  }

  @override
  int get hashCode => value.hashCode;
}

class DeviceInfo {
  final String? deviceId;
  final DeviceType? deviceType;
  final String? idfa;
  final String? osVersion;
  final String? machine;
  final bool? supportTelephony;
  final NetworkInfo? network;

  //上一次uuid创建时间
  //只有非首次安装后首次打开有数据
  final int? deviceIdCreateTime;

  DeviceInfo.fromMap(Map map)
      : deviceId = map['deviceId'],
        deviceIdCreateTime = map['deviceIdCreateTime'],
        deviceType = DeviceType(map['deviceType']),
        idfa = map['idfa'],
        osVersion = map['OSVersion'],
        machine = map['machine'],
        supportTelephony = map['supportTelephony'],
        network =
            map['network'] != null ? NetworkInfo.fromMap(map['network']) : null;
}
