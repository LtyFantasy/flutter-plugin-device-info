import 'package:flutter/material.dart' hide Action;
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:device_info_plugin/device_info_plugin.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
    initDeviceInfoState();
    initNetworkInfoState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await DeviceInfoPlugin.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  Future initDeviceInfoState() async {
    try{
      DeviceInfo deviceInfo = await DeviceInfoPlugin.deviceInfo;
      debugPrint("device: ${deviceInfo.deviceId}");
    } on PlatformException {
      debugPrint('Failed to get deviceInfo.');
    }
  }


  Future initNetworkInfoState() async {
    try{
      NetworkInfo networkInfo = await DeviceInfoPlugin.networkInfo;
      debugPrint("networkInfo: ${networkInfo.connection}");
    } on PlatformException {
      debugPrint('Failed to get deviceInfo.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child:FutureBuilder(
            future: DeviceInfoPlugin.deviceInfo,
            builder: (BuildContext context, AsyncSnapshot<DeviceInfo> snapshot){
              if(snapshot.hasData){
                return  Text('deviceInfo on: ${snapshot.data.deviceId}\n'
                    '${snapshot.data.deviceIdCreateTime}');
              }
              return  Text('Running on: $_platformVersion\n');
            },
          ),
        ),
      ),
    );
  }
}
