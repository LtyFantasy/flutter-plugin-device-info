#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"
#import <device_info_plugin/TNDeviceInfo.h>
#import <device_info_plugin/TNNetworkInfo.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GeneratedPluginRegistrant registerWithRegistry:self];
    // Override point for customization after application launch.
    
    
    NSLog(@"%@", [TNDeviceInfo current]);
    
    NSLog(@"%@", [TNNetworkInfo info]);
    
    return true;
}

@end
